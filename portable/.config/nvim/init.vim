
" NB: les réglages spécifiques à un type de fichier vont dans :
" ~/.config/nvim/after/ftplugin/filetype_name.vim

" NB: on peut paramétrer des détails de formatage de texte avec formatoptions
" ça a l'air bien, faut regarder dans l'aide

" PLUGINS MANAGER {{{
call plug#begin('~/.local/share/nvim/plugged')

" Les trucs jolis
Plug 'nvim-lualine/lualine.nvim' "status bar

Plug 'akinsho/bufferline.nvim', { 'tag': '*' } "bar d'onglets

" Color scheme
Plug 'dracula/vim',{'as':'dracula'}
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
Plug 'catppuccin/nvim', {'as': 'catppuccin'}

Plug 'lukas-reineke/indent-blankline.nvim' "guide d'indentation

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

"devicons
Plug 'kyazdani42/nvim-web-devicons'

"LSP autocomplete
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'
Plug 'ray-x/cmp-treesitter'
Plug 'f3fora/cmp-spell'

"Native LSP
Plug 'neovim/nvim-lspconfig' " série de settings pour rendre l'intégration de LSP plus simple
Plug 'williamboman/nvim-lsp-installer' " aide pour installer les serveurs
Plug 'jose-elias-alvarez/null-ls.nvim' " complète les LSP avec d'autres outils. p. ex le LSP lua n'a pas de formatteur, donc quant on on appel la commande vim.lsp.buf.formatting(), rien ne se passe. Avec ce plug-in ça fonctionne

Plug 'WhoIsSethDaniel/toggle-lsp-diagnostics.nvim'

"Telescope and Requirements
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

"Better Telescope sorter
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }

"Telescope filebrowser
Plug 'nvim-telescope/telescope-file-browser.nvim'


" Autoclose parenthèses etc.
Plug 'jiangmiao/auto-pairs'
"let g:AutoPairsMultilineClose=0

" Commentaire de ligne <leader>c<space>
"Plug 'scrooloose/nerdcommenter'
Plug 'terrortylor/nvim-comment' " toggle comment avec gcc ou selction plus gc

" Syntax pour languages
"Plug 'stephpy/vim-yaml'
"Plug 'pearofducks/ansible-vim'

" XML
"Plug 'sukima/xmledit'
"Plug 'Valloric/MatchTagAlways'

" Markdown
"Plug 'vim-pandoc/vim-pandoc'
"Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'jakewvincent/mkdnflow.nvim'

" VIMWIKI
"Plug 'vimwiki/vimwiki'
"let g:vimwiki_list = [{'path': '~/doc/vimwiki/', 'path_html': '~/doc/vimwiki_web'}]
let g:markdown_fenced_languages = ['html', 'python', 'php', 'vim', 'js=javascript', 'bash']

Plug 'ap/vim-css-color'


call plug#end()


" }}}

" MAPPINGS {{{
" Loosy shift when saving
cmap Wq wq
cmap Q q
cmap W w

" Cancel search highlight
nnoremap <leader>h :noh<cr>

" Déplacement vertical dans les lignes wrappées
nnoremap j gj
nnoremap k gk

" Space ouvre les "pliages de code mais les ferme pas. Avec ça, oui.
nnoremap <leader><space> za

" DEPLACER des truc séléctionnés
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" Remap search with dash because QWERTY-CH
vnoremap - /
nnoremap - /

" Simplification de la navigation dans l'AIDE
"    Press Enter to jump to the subject (topic) under the cursor.
"    Press Backspace to return from the last jump.
"    Press s to find the next subject, or S to find the previous subject.
"    Press o to find the next option, or O to find the previous option.

autocmd FileType help nnoremap <buffer> <CR> <C-]>
autocmd FileType help nnoremap <buffer> <BS> <C-T>
autocmd FileType help nnoremap <buffer> o /'\l\{2,\}'<CR>
autocmd FileType help nnoremap <buffer> O ?'\l\{2,\}'<CR>
autocmd FileType help nnoremap <buffer> s /\|\zs\S\+\ze\|<CR>
autocmd FileType help nnoremap <buffer> S ?\|\zs\S\+\ze\|<CR>

" Plugin toggle-lsp-diagnostics
nnoremap <leader>td :ToggleDiag<cr>

" }}}

" LEADER
"let mapleader = "<space>"
" En mettant ce mapping plutôt que le précédent, on voit dans la ligne de
" commande la commande que l'on a appelée
map <space> <leader>

" COLOR SCHEME
"let g:tokyonight_style = "night"
"colorscheme catppuccin
colorscheme tokyonight
"colorscheme dracula
set background=dark
set t_Co=256

" Execute Python script
" Shell est une fonction perso qui ouvre un buffer et colle l'output du script
" dedans. On peut ajouter <C-w> à la fin de la commande pour pouvoir
" directement fermer le buffer avec c
autocmd FileType python map <buffer> <F9> :w<CR>:Shell %:p<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

" SETTINGS {{{
set mouse=a 	 		        " Enable mouse
set autowriteall 			        " Save file when switching buffer
set cursorline 			        " Surligage ligne du curseur
set termguicolors	 	        " Nottament pour que les couleurs du shema fonctionnent
set backspace=indent,eol,start	" Fix backspace indent
set clipboard=unnamedplus       " Copy paste between vim and everything else Nécésite xclip
set autoindent
set scrolljump=-15              " Accelerate scrolling
set noshowmode                  " Enlève la mention --INSERT-- en dessous de la status bar
set noshowcmd                   " Ne montre pas la dernière commande dans la bare de commande
set number                      " Numérotation des lignes
set splitright                  " les split verticaux se créent sur la droite
set background=dark
set foldmethod=marker

" Tab shenanigans
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set smarttab

" Can undo after reopening the file
set undofile
set undodir=/tmp

" Devient sensible à la casse que si on tappe des majuscules
set ignorecase
set smartcase

" ligne de commande fait 2 lignes
set cmdheight=2


" }}}

" FONCTIONS {{{
" Auto source when writing to init.vm alternatively you can run :source $MYVIMRC
au! BufWritePost $MYVIMRC source %

" Permutte entre numéros de ligne relatifs (Normal mode) et absolus (Insert mode)
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Automatically deletes all trailing whitespace on save.
autocmd BufWritePre * %s/\s\+$//e


" Change la date du frontmatter des fichiers markdown
autocmd BufWritePre *.md %s/^\(date: \)\d\d\d\d-\d\d-\d\d/\=submatch(1).strftime("%Y-%m-%d")/e

" Ouvre un buffer et colle le résultat de l'exécution d'une commande
command! -complete=file -nargs=+ Shell call s:runshellcommand(<q-args>)
function! s:runshellcommand(cmdline)
  botright vnew
  setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
  call setline(1,a:cmdline)
  call setline(2,substitute(a:cmdline,'.','=','g'))
  execute 'silent $read !'.escape(a:cmdline,'%#')
  setlocal nomodifiable
  1
endfunction

" Autosave when focus is lost
:au FocusLost * :wa

" }}}

au bufNewFile,BufRead *.{xsl,xslt} set filetype=xml

" Config syntax plugin Ansible
let g:ansible_attribute_highlight = "ob"
let g:ansible_name_highlight = 'd'

" Config vim-pandoc
let g:pandoc#spell#enabled = 'false' " c'est le LSP ltex qui s'en occupe.
let g:pandoc#folding#level = 2
let g:pandoc#folding#fdc = 0

lua require("config")
