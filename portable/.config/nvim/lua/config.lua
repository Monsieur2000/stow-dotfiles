vim.opt.termguicolors = true

-- LSP config

local capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())

-- LspInstall plugin config
local lsp_installer = require("nvim-lsp-installer")

local opts = { noremap = true, silent = true }

local function on_attach(client, bufnr)
	-- Set up buffer-local keymaps (vim.api.nvim_buf_set_keymap()), etc.
	vim.api.nvim_buf_set_keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "gd", "<cmd>lua vim.lsp.buf.definition()<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "gt", "<cmd>lua vim.lsp.buf.type_definition()<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "gi", "<cmd>lua vim.lsp.buf.implementation()<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>j", "<cmd>lua vim.diagnostic.goto_next()<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>dk", "<cmd>lua vim.diagnostic.goto_prev()<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>dl", "<cmd>Telescope diagnostics<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>r", "<cmd>lua vim.lsp.buf.rename()<cr>", opts)
	vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)

    vim.opt.signcolumn = "yes" -- Evite que la goutière des symbols de diagnotique disparaise en mode insertion
end


-- Register a handler that will be called for each installed server when it's ready (i.e. when installation is finished
-- or if the server is already installed).
lsp_installer.on_server_ready(function(server)
	-- Specify the default options which we'll use to setup all servers
	local opts = {
		capabilities = capabilities,
		on_attach = on_attach,
	}

	-- (optional) Customize the options passed to the server
	if server.name == "ltex" then
		opts.settings = {
			ltex = {
				language = "fr",
				completionEnabled = true,
			},
		}
        opts.filetypes = { "pandoc" } -- le plugin vim-pandoc change le filetype des "markdown" en "pandoc". Par contre, je pense que ça override les défault. Donc il faudrait ajouter lesextension latex.
	end

	-- This setup() function will take the provided server configuration and decorate it with the necessary properties
	-- before passing it onwards to lspconfig.
	-- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
	server:setup(opts)
end)

-- LSP autocomplete
vim.opt.completeopt = { "menu", "menuone", "noselect" } -- setting vim values

-- LSP : supprime les diagnostics inline
-- vim.diagnostic.config({
--   virtual_text = false
-- })

-- LSP: Show diagnostics automatically in hover window
-- vim.o.updatetime = 1500
-- vim.cmd [[autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]

-- LSP use sighs instead of letters
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

-- Set spelling options
vim.opt.spell = true
vim.opt.spelllang = { 'fr_fr', 'en_us' }

-- Setup nvim-cmp.
local cmp = require("cmp")

cmp.setup({
	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
		end,
	},
	mapping = cmp.mapping.preset.insert({
		["<C-b>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.abort(),
		["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
	}),
	sources = cmp.config.sources({
		{ name = "nvim_lsp" },
		{ name = "luasnip" }, -- For luasnip users.
        { name = "treesitter" },
        { name = "spell" }
	}, {
		{ name = "buffer" },
	}),
})



-- Setup Telescope
require("telescope").setup({
	defaults = {
		vimgrep_arguments = {
			"rg",
			"--color=never",
			"--no-heading",
			"--with-filename",
			"--line-number",
			"--column",
			"--smart-case",
		},
		prompt_prefix = "   ",
		selection_caret = "  ",
		entry_prefix = "  ",
		initial_mode = "insert",
		selection_strategy = "reset",
		sorting_strategy = "ascending",
		layout_strategy = "horizontal",
		layout_config = {
			horizontal = {
				prompt_position = "top",
				preview_width = 0.55,
				results_width = 0.8,
			},
			vertical = {
				mirror = false,
			},
			width = 0.87,
			height = 0.80,
			preview_cutoff = 120,
		},
		file_sorter = require("telescope.sorters").get_fuzzy_file,
		file_ignore_patterns = { "node_modules" },
		generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
		path_display = { "truncate" },
		winblend = 0,
		border = {},
		borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
		color_devicons = true,
		use_less = true,
		set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
		file_previewer = require("telescope.previewers").vim_buffer_cat.new,
		grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
		qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
		-- Developer configurations: Not meant for general override
		buffer_previewer_maker = require("telescope.previewers").buffer_previewer_maker,
	},
})

-- Setup lignes indentation (blankline)
require("indent_blankline").setup({
	show_current_context_start = true,
	show_current_context = true,
	use_treesitter = true,
	show_first_indent_level = false,
})

-- Load Telescope add-ons
require("telescope").load_extension("file_browser")
require("telescope").load_extension("fzf")

require("null-ls").setup({
	sources = {
		require("null-ls").builtins.formatting.stylua,
		--require("null-ls").builtins.diagnostics.eslint,
		--require("null-ls").builtins.completion.spell,
	},
})

require('lualine').setup {
    options = {
        section_separators = '',
        component_separators = '',
        theme = "tokyonight",
    }
}

require("bufferline").setup{
    options = {
        show_buffer_close_icons = false,
        show_close_icon = false,
        modified_icon = '·',
    }
}

require'toggle_lsp_diagnostics'.init()

require('mkdnflow').setup({})

require('nvim_comment').setup()
